import { LocalScheme } from '~auth/runtime'

export default class LumenPassport extends LocalScheme {
  async login(endpoint, { reset = true } = {}) {
    if (!this.options.endpoints.login) {
      return
    }
    if (reset) {
      this.$auth.reset({ resetInterceptor: false })
    }
    if (this.options.clientId) {
      endpoint.data.client_id = this.options.clientId
    }
    if (this.options.clientSecret) {
      endpoint.data.client_secret = this.options.clientSecret
    }
    if (this.options.grantType) {
      endpoint.data.grant_type = this.options.grantType
    }
    if (this.options.scope) {
      endpoint.data.scope = this.options.scope
    }
    const response = await this.$auth.request(
      endpoint,
      this.options.endpoints.login
    )
    this.updateTokens(response)
    if (!this.requestHandler.interceptor) {
      this.initializeRequestInterceptor()
    }
    if (this.options.user.autoFetch) {
      await this.fetchUser()
    }
    return response
  }
}
